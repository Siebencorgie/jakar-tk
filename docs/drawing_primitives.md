# Drawing Primitives
## What does it do?
The `DrawingPrimitives` module is used to draw primitives of the UI such as:
- Text
- Surfaces
- SurfacesEdge
- Image (Draws an image)

The module creates a needed pipeline and knows which uniform_buffers or other buffer to build to draw a certain item. Within a `Widget`
implementation those primitives can be used to create a new widget, which in turn create the full UI.

## Implementing my own widget
If you want to use primitives like a `Text` changes are that you'll need a buffer which will contain the text which is send to the GPU.
The primitive module also handles this buffer generation. You therefore also need to call `.get_text_buffer()` which will return a ID to you.
The Id is passed to the `draw_text(id: ID)` function to render the text. If you have dynamic text for instance a editor window you can also call `draw_dynamic_text()` which will create a text buffer at render_time. However, be aware that this can potentially slow down your UI for big buffer.

pub extern crate winit;
#[macro_use]
pub extern crate vulkano;
#[macro_use]
pub extern crate vulkano_shader_derive;
pub extern crate vulkano_shaders;
pub extern crate vulkano_win;

///Defines the renderer for the windows. The renderer can either be build stand alone, or use already created
/// Vulkano systems (mainly device, queue and instance).
pub mod render_host;
///Defines the central widget trait. Also has sub modules which implement the most common widgets, like a button or layouts.
pub mod widget;




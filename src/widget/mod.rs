use vulkano::command_buffer::AutoCommandBufferBuilder;


///Must be implemented by any widget that can be inserted into a widget tree
pub trait Widget{
    ///Draws this widget and its subwidgets. Writes to the supplied `command_buffer`.
    ///Assumes that the CommandBufferBuilder is within a RenderPass which is the same as the widgets pipeline.
    fn update(&mut self, command_buffer: AutoCommandBufferBuilder);
    ///Sets the region of the final frame buffer, to which this widget should be drawn.
    fn set_region(&mut self, origin: [u32; 2], size: [u32; 2]);
}

